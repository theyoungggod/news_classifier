import pytest
import json
import requests


def _get_dict_from_file(filename):
    with open(filename) as file:
        dict_obj = json.load(file)
        return dict_obj


def _get_dict_from_response():
    arr_res_data = []
    test_samples = _get_dict_from_file('../data/test_samples.json')
    for test_sample in test_samples:
        response_json = requests.post('http://127.0.0.1:8000', json=test_sample).text
        dict_res_data = {}
        dict_res_data["sample"] = test_sample
        dict_res_data["result"] = json.loads(response_json)
        arr_res_data.append(dict_res_data)
    return arr_res_data


def test_():
    test_data = _get_dict_from_file('../data/test_data.json')
    res_data = _get_dict_from_response()
    for i in range(len(res_data)):
        try:
            assert res_data[i] == test_data[i]
        except AssertionError:
            print(f"what had to be:\n{json.dumps(test_data[i], ensure_ascii=False, indent=4)}" +\
                f"\nwhat is in the response:\n{json.dumps(res_data[i], ensure_ascii=False, indent=4)}\n")
            pass    


if __name__ == "__main__":
    test_()
