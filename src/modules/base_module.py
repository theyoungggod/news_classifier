from src.modules.slicer import slicer
from src.modules.sentiment_model import SentimentModel
from src.modules.article_classifier import article_classifier

model = SentimentModel()


def base_module(text, tag, context_size):
    pos_passages = []
    neg_passages = []
    neu_passages = []
    labels_of_text = []
    passages = slicer(text, tag, context_size)
    for passage in passages:
        label_of_passage = model(passage)
        # находим лайбел для каждого пассажа

        if label_of_passage == "positive":
            pos_passages.append(passage)
        elif label_of_passage == "negative":
            neg_passages.append(passage)
        else:
            neu_passages.append(passage)

        labels_of_text.append(label_of_passage)
        # добавляем все лейблы в общий массив
    count_of_positive = labels_of_text.count("positive")
    count_of_negative = labels_of_text.count("negative")
    count_of_neutral = labels_of_text.count("neutral")
    arcticle_stats = {
        "n_pos": count_of_positive,
        "n_neg": count_of_negative,
        "n_neu": count_of_neutral
    }
    arcticle_label = article_classifier(arcticle_stats)
    result = {
        "text": text,
        "tag": tag,
        "article_label": arcticle_label,
        "pos_passages": pos_passages,
        "neg_passages": neg_passages,
        "neu_passages": neu_passages,
        "n_pos": count_of_positive,
        "n_neg": count_of_negative,
        "n_neu": count_of_neutral
    }
    return result
