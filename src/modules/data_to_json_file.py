import json


def data_to_json_file(data: json):
    with open('./data/data.json', 'a') as file:
        print(data)
        json.dump(data, file, ensure_ascii=False, indent=4)
