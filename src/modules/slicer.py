import nltk
nltk.download('punkt')
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize


def split_into_paragr(text):
  return [paragr for paragr in text.split("\n") if len(paragr) > 0]


def check_tag(tags, sentence):
  arr_tags = word_tokenize(tags.lower())
  sentence_word_tokenized = word_tokenize(sentence.lower())
  for tag in arr_tags:
    if tag.lower() not in sentence_word_tokenized:
      return False
  return True


def context_sentences(sentences, id, context_size):
  start_id = max(id - context_size, 0)
  end_id = id + context_size + 1
  return sentences[start_id:end_id]


def slicer_paragr(paragr, tag, context_size):
  paragr_with_tag = []
  sentences = sent_tokenize(paragr)
  for id, sentence in enumerate(sentences):
    if check_tag(tag, sentence):
      paragr_with_tag.append(' '.join(context_sentences(sentences, id, context_size)))
  return paragr_with_tag


def slicer(text, tag, context_size):
  text_with_tag = []
  paragr_lst = split_into_paragr(text)
  for paragr in paragr_lst:
    text_with_tag.extend(slicer_paragr(paragr, tag, context_size))
  return text_with_tag
