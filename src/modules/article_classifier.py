def article_classifier(Dict):
    n_neg = Dict["n_neg"]
    n_pos = Dict["n_pos"]
    if n_neg > n_pos:
        return("neg")
    if n_neg < n_pos:
        return("pos")
    else:
        return("neu")
