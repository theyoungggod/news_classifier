import numpy as np
import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification


class SentimentModel:
    """
    example of use:
    >>> sentimentModel = SentimentModel()
    >>> sentimentModel("Привет Мир!")
    array(['neutral'])

    >>> sentimentModel(["Это плохо", "Это хорошо"])
    array(['negative', 'positive'])
    """
    def __init__(self, 
                 tokenizer_name = "blanchefort/rubert-base-cased-sentiment", 
                 model_name = "blanchefort/rubert-base-cased-sentiment",
                 device = None):
        
        self.tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
        self.model = AutoModelForSequenceClassification.from_pretrained(model_name)

        if not device:
            device = 'cuda' if torch.cuda.is_available() else 'cpu'
        
        self.device = device

        self.model.to(self.device)

    @torch.no_grad()
    def __call__(self, texts, max_length=512):
        inputs = self.tokenizer(texts, max_length=max_length, padding=True,
                                truncation=True, return_tensors='pt')
        inputs = inputs.to(self.device)
        
        outputs = self.model(**inputs)
        
        predicted = torch.nn.functional.softmax(outputs.logits, dim=1)
        predicted = torch.argmax(predicted, dim=1).cpu().numpy()
        
        out = np.array(['neutral', 'positive', 'negative'])[predicted]
        
        return out