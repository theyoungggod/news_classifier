from datetime import datetime
import json


class Logger:
    def __init__(self, keys_order=[], log_file="service.log"):
        self.keys_order = keys_order
        self.log_file = log_file

    def _log_line(self, line):
        print(line)
        with open(self.log_file, 'a') as logs:
            logs.write(line + '\n')
    
    def _log_in_out_dicts_to_json(self, service_input, service_output, is_error):
        with open(self.log_file + ".json", "a") as jsonlogs:
            jsonlogs.write(json.dumps({"timestamp":self._get_current_time(), "in":service_input, "out":service_output, "is_error":is_error}) + "\n")
    
    def _get_current_time(self):
        return datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    def _get_message_from_dict(self, input_dict):
        if not self.keys_order:
            keys = sorted(input_dict.keys())
        else:
            keys = self.keys_order
        return [key + ": " + str(input_dict.get(key)) for key in keys if input_dict.get(key) is not None]
    
    def write_log(self, service_input, service_output, is_error=False):
        if is_error:
            self._log_line('@' * 30)
            self.keys_order = []
        else:
            self._log_line('=' * 30)
        messages = ["In:"] + self._get_message_from_dict(service_input) + ["Out:"] + self._get_message_from_dict(service_output)
        for message in messages:
            self._log_line(self._get_current_time() + '\t' + message)
        self._log_in_out_dicts_to_json(service_input, service_output, is_error)
