from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware

from src.tools.logger import Logger
from src.modules.base_module import base_module
from src.modules.data_to_json_file import data_to_json_file
from src.models.models import DictIn, DictOut


app = FastAPI()
logger = Logger()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],)


@app.post('/')
def get_base_module(data: DictIn) -> DictOut:
    result_dict = base_module(**data.dict())

    logger.write_log(dict(data), result_dict)

    return DictOut(**result_dict)


@app.post('/data')
async def post_data(data: Request):
    data_json = await data.json()
    data_to_json_file(data_json)
    return data_json
