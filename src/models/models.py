from pydantic import BaseModel, validator
from typing import List


class DictIn(BaseModel):
    text: str
    tag: str
    context_size: int

    @validator('text', 'tag')
    def must_be_not_empty(cls, val):
        if val == '':
            raise ValueError('Text and tag must be not empty')
        return val
    
    @validator('context_size')
    def not_negative(cls, context_size):
        if context_size < 0:
            raise ValueError('Context size must be not negative')
        return context_size


class DictOut(BaseModel):
    text: str
    tag: str
    article_label: str
    pos_passages: List[str]
    neg_passages: List[str]
    neu_passages: List[str]
    n_pos: int
    n_neg: int
    n_neu: int
